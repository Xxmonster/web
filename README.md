## `开发工具：vscode`
### 编辑器及插件安装教程(https://www.cnblogs.com/csji/p/13558221.html)
### 常用快捷键(不用强记，多用自然会记住)：
- Ctrl + X 剪切
- Ctrl + C 复制
- Ctrl + N 新建文件
- Ctrl + O 打开文件
- Ctrl + S 保存文件
- Ctrl + Shift + S 另存为
- Ctrl + Shift + N 打开新的编辑器窗口
- Home 光标跳转到行头
- End 光标跳转到行尾
- Ctrl + Home 跳转到页头
- Ctrl + End 跳转到页尾
- Alt + ↑ / ↓ 移动行上下
- Shift + Alt ↑ / ↓ 在当前行上下复制当前行
- Ctrl + Shift + K 删除行
- Ctrl + ] 或 [ 行缩进
- Ctrl + Enter 在当前行下插入新的一行
- Ctrl + Shift + Enter 在当前行上插入新的一行
- Ctrl + Shift + [ 折叠区域代码
- Ctrl + Shift + ] 展开区域代码
- Ctrl + F 查询
- Ctrl + H 替换
### 常用插件：
- Chinese vscode汉化插件
- Open in Browser 在浏览器中打开html文件(右键-->Open in Browser)
- Auto Close Tag 自动闭合HTML/XML标签
- Auto Rename Tag 自动完成另一侧标签的同步修改
- HTML Snippets 智能提示HTML标签，以及标签含义
- HTML CSS Support 智能提示CSS类名class以及id

## `教程视频`：
1. [HTML+CSS](https://www.bilibili.com/video/BV1pE411q7FU?spm_id_from=333.788.b_636f6d6d656e74.7)
2. [JavaScript](https://www.bilibili.com/video/BV1ux411d75J?spm_id_from=333.788.b_636f6d6d656e74.8)
3. [DOM(文档对象模型/Document Object Model) BOM(浏览器对象模型/Browser Object Model)](https://www.bilibili.com/video/BV1k4411w7sV?spm_id_from=333.788.b_636f6d6d656e74.9)
4. [JQuery + Bootstrap](https://www.bilibili.com/video/BV1a4411w7Gx?spm_id_from=333.788.b_636f6d6d656e74.10)
## `各类手册查询`：https://www.runoob.com